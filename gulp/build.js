'use strict';

var gulp = require('gulp');

var $ = require('gulp-load-plugins')();
var saveLicense = require('uglify-save-license');
var mainBowerFiles = require('main-bower-files');

gulp.task('styles', function () {
    return gulp.src(['app/styles/*.css'])
        .pipe($.plumber())
        .pipe($.autoprefixer('last 1 version'))
        .pipe(gulp.dest('.tmp/styles'))
        .pipe($.size());
});

gulp.task('scripts', function () {
    return gulp.src(['app/scripts/**/*.js'])
        // .pipe($.jshint())
        // .pipe($.jshint.reporter('jshint-stylish'))
        .pipe($.size());
});

gulp.task('datepicker', function () {
    return gulp.src(['app/datepicker/*.html'])
        .pipe($.minifyHtml({
            empty: true,
            spare: true,
            quotes: true
        }))
        .pipe(gulp.dest("dist/datepicker"))
        .pipe($.size());
});

gulp.task('tooltip', function () {
    return gulp.src(['app/tooltip/*.html'])
        .pipe($.minifyHtml({
            empty: true,
            spare: true,
            quotes: true
        }))
        .pipe(gulp.dest("dist/tooltip"))
        .pipe($.size());
});

gulp.task('partials', function () {
    return gulp.src(['app/partials/**/*.html'])
        .pipe($.minifyHtml({
            empty: true,
            spare: true,
            quotes: true
        }))
        .pipe(gulp.dest("dist/partials"))
        .pipe($.size());
});

gulp.task('html', ['styles', 'scripts', 'datepicker', 'tooltip' , 'partials', 'fonts' , 'images'], function () {
    var jsFilter = $.filter('**/*.js');
    var cssFilter = $.filter('**/*.css');
    var fontFilter = $.filter('**/*.{eot,svg,ttf,woff}');

    return gulp.src('app/*.html')
        // .pipe($.useref.assets())
        .pipe($.useref())
        .pipe(jsFilter)
        .pipe($.ngmin())
        .pipe($.rev())
        .pipe($.uglify({preserveComments: saveLicense}))
        .pipe(jsFilter.restore())
        .pipe(cssFilter)
        .pipe($.replace('bower_components/bootstrap/fonts', 'fonts'))
        .pipe($.cleanCss())
        .pipe(cssFilter.restore())
        // .pipe($.useref.restore())
        // .pipe($.useref())
        .pipe(fontFilter)
        .pipe(fontFilter.restore())
        .pipe($.revReplace())
        .pipe(gulp.dest('dist'))
        .pipe($.size());
});

gulp.task('images', function () {
    return gulp.src(['app/images/**/*'])
        .pipe($.imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest('dist/images'))
        .pipe($.size());
});

gulp.task('fonts', function () {
    return gulp.src(['app/fonts/**/*'])
        .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
        .pipe($.flatten())
        .pipe(gulp.dest('dist/fonts'))
        .pipe($.size());
});

gulp.task('clean', function () {
    return gulp.src(['.tmp', 'dist'], {read: false}).pipe($.clean());
});

gulp.task('clean:cache', function (done) {
    return $.cache.clearAll(done);
});

gulp.task('build', ['html', 'partials', 'images']);
