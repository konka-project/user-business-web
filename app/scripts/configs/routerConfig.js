/**
 * Created by yangdeng on 16/9/1.
 */
'use strict';
(function () {

    var routerConfig = function ($stateProvider, $urlRouterProvider) {
        
        $urlRouterProvider.otherwise('/main/active/userEvent');

        $stateProvider
            .state('left', {
                url: '/main',
                templateUrl: 'partials/leftNavManage/leftNav.html',
                controller: 'leftNav'
            })
            .state('left.active', {
                url: '/active/:id',
                templateUrl: 'partials/mainManage/main.html',
                controller: 'rightMain'
            })
            .state('left.active-detail', {
                url: '/active/:id/detail/:type',
                templateUrl: 'partials/mainManage/detail.html',
                controller: 'rightDetail'
            })
            .state('left.active-add', {
                url: '/active/:id/add/:add',
                templateUrl: 'partials/mainManage/add.html',
                controller: 'rightAdd'
            })
            .state('left.active-edit', {
                url: '/active/:id/edit/:edit',
                templateUrl: 'partials/mainManage/edit.html',
                controller: 'rightEdit'
            })
            .state('left.active.choice', {
                url: '/:choice',
                templateUrl: 'partials/mainManage/main.html',
                controller: 'userEvent'
            })

            .state('left.signUnDetail',{
                url: '/signUpAccount/detail',
                templateUrl: 'partials/signUpAccount/signUpAccountDetail.html',
                controller: 'signUpAccountDetail'
            });
    };

    routerConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    angular.module('myApp.router.config', [])
        .config(routerConfig);

})();
