'use strict';
(function () {
    angular.module('myApp', [
        'ui.router',
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'mgcrea.ngStrap',
        'gridster',
        'selectize',
        'ui.bootstrap',

        'myApp.configs',
        'myApp.services', // Services
        'myApp.controllers', // controller
        'myApp.directives'
    ]);
})();
