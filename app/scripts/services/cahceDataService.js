'use strict';
(function(){
	
	var cacheService = function(){
		var _self = this;
		this.cacheData = {};
		this.getCacheData = function(key){
			return _self.cacheData[key];
		};
		this.setCacheData = function(key,data){
			_self.cacheData = cacheData[key];
		};
	};

	cacheService.$inject = [];

	angular.module('cache.service',[])
		.service('cacheService',cacheService);

})();