/**
 * Created by yangdeng on 16/9/1.
 */
'use strict';
(function () {

    var deleteSureService = function ($modal, $q) {

        this.open = function (deleteRemainSesten, deleteRemainDownSesten ) {

            var promise = $q.defer();

            var _modal = $modal({
                templateUrl: 'partials/modal/deleteSure.html',
                backdrop: 'static',
                placement: 'center',
                show: true,
                keyboard: false,
                animation: 'am-fade',
                backdropAnimation: 'am-fade'
            });


            var scope = _modal.$scope;

            scope.deleteRemainSesten = deleteRemainSesten;
            scope.deleteRemainDownSesten = deleteRemainDownSesten;

            scope.modalEventDecideChoice = function () {
                scope.$hide();
                promise.resolve(true);
            };

            scope.cancel = function () {
                scope.$hide();
                promise.resolve(false);
            };

            return promise.promise;

        };

    };

    deleteSureService.$inject = ['$modal', '$q'];

    angular.module('myApp.delete.sure.modal', [])
        .service('deleteSureService', deleteSureService);

})();