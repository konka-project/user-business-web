"use strict";
(function() {
    var successOperation = function($modal,$timeout) {

        this.open = function(successDetail) {
            var _modal = $modal({
               templateUrl: 'partials/modal/successOperation.html',
                backdrop: 'static',
                placement: 'center',
                show: true,
                keyboard: false,
                animation: 'am-fade',
                backdropAnimation: 'am-fade'
            });
            var scope = _modal.$scope;
            scope.successDetail =successDetail;
            scope.successShow = function() {
                $timeout(function() {
                    
                    scope.$hide();
                },1000);
            };
            scope.successShow();
        }

    };
    successOperation.$inject = ['$modal','$timeout'];
    angular.module("myApp.success.operate.modal",[])
        .service("successOperation",successOperation);
})();