/**
 * Created by yangdeng on 16/9/5.
 */
'use strict';
(function () {

    var editMedal = function ($modal) {

        this.open = function () {
            var _modal = $modal({
                templateUrl: 'partials/modal/editMedal.html',
                backdrop: 'static',
                placement: 'center',
                show: true,
                keyboard: false,
                animation: 'am-fade',
                backdropAnimation: 'am-fade'
            });

            var scope = _modal.$scope;
        };

    };

    editMedal.$inject = ['$modal'];

    angular.module('edit.medal.modal',[])
        .service('editMedal',editMedal);

})();