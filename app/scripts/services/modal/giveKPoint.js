/**
 * Created by yangdeng on 16/9/5.
 */
'use strict';
(function () {

    var giveKPoint = function ($modal) {
        
        this.open = function () {
            var _modal = $modal({
                templateUrl: 'partials/modal/giveKPoint.html',
                backdrop: 'static',
                placement: 'center',
                show: true,
                keyboard: false,
                animation: 'am-fade',
                backdropAnimation: 'am-fade'
            });

            var scope = _modal.$scope;

            scope.cancel = function (e) {
                e.stopPropagation();
                scope.$hide();
            };

            scope.modalEventDecideChoice = function (e) {
                e.stopPropagation();
                scope.$hide();
            }
        };
        
    };

    giveKPoint.$inject = ['$modal'];

    angular.module('giveKPoint.modal',[])
        .service('giveKPoint',giveKPoint);

})();