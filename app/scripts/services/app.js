'use strict';
(function(){

	angular.module('myApp.services',[
		'cache.service',
		'myApp.success.operate.modal',
		'myApp.delete.sure.modal',
		'giveKPoint.modal',
		'edit.medal.modal'
	]);


})();