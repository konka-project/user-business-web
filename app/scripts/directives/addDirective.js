/*
 * Directives
 */
'use strict';
(function() {
    var directiveModule = angular.module('myApp.rightAdd.directive', []);

    directiveModule.directive('rightAdd', function() {
        return {
            restrict: 'A',
            templateUrl:'partials/headNavManage/add.html',
            controller: ['$scope',function($scope){

            }]
        };
    });
})();
