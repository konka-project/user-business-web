/*
 * Directives
 */
'use strict';
(function() {
    var directiveModule = angular.module('myApp.header.directive', []);

    directiveModule.directive('header', function() {
        return {
            restrict: 'A',
            templateUrl:'partials/headNavManage/headNav.html',
            controller: ['$scope',function($scope){

            }]
        };
    });
})();
