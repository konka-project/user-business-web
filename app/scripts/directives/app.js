'use strict';
(function(){

	angular.module('myApp.directives',[
			'myApp.header.directive',
			'left.nav.directive',
			'myApp.footer.directive',
			'file.upload.directive'
	]);
})();