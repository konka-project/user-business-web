'use strict';
(function() {
	angular.module('myApp.footer.directive',[])
	.directive('footer', function() {
        return {
            restrict: 'A',
            templateUrl:'partials/footerManage/footer.html',
            controller: ['$scope',function($scope){

            }]
        };
    });
})();