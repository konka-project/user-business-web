'use strict';
(function(){

	var fileUpload = function(){
		return {
			restrict: 'A',
			scope: {
				file: '=ngModel'
			},
			link:function(scope,ele,attr){
				ele.bind('change',function(){
					scope.file = ele[0].files[0];
					// scope.file = (event.srcElement || event.target).files[0];
					// scope.getFile();
					scope.$apply();
				});
			}
		};
	}

	fileUpload.$inject = [];

	angular.module('file.upload.directive',[])
		.directive('fileUpload',fileUpload);


})();