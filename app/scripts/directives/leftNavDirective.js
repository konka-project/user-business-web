/*
 * Directives
 */
'use strict';
(function(){
var directiveModule = angular.module('left.nav.directive', []);
/**
* $scope 定义单个controller和html的桥梁
* $rootstate 可以在所有的controller中使用
* $state 路由机制
*/
directiveModule
	.controller('leftNav',['$scope','$state','$rootScope',function($scope,$state,$rootScope) {
        $rootScope.pageLineNumber = [
            {
                value:15,
                name:'15条'
            },{
                value:25,
                name:'25条'
            },{
                value:50,
                name:'50条'
            },{
                value:100,
                name:'100条'
            }
        ];
		$rootScope.leftNavChoose = $state.params.id;
        		$rootScope.navList = [
                    {
                        name: '注册用户管理',
                        value: 'signUpAccount'
                    },{
        				name: '忠诚度管理',
        				value: 'loyaltyManage',
                        sonUl: [{
                            name: "用户事件",
                            value: "userEvent"
                        },{
                            name: "任务管理",
                            value: "taskManage"
                        },{
                            name: "勋章管理",
                            value: "medalManage"
                        },{
                            name: "特权管理",
                            value: "privilegeManage"
                        },{
                            name:"卡券管理",
                            value:"cardManage"
                        },{
                            name:"等级管理",
                            value:"gradeManage"
                        }]
        			},
        			{
        				name: '活动管理',
        				value: 'activityOperate'
        			},
        			{
						name: '运营统计',
						value: 'operateStatics'
        			},
        			{
        				name: '保修管理',
        				value: 'warrantyManage'
        			},
        			{
    					name: 'k点商城',
    					value: 'kShopping'
        			},
        			{
        				name: '推送管理',
        				value: 'pushManage'
        			}
        		];

    			$rootScope.navMap = {
                    'signUpAccount': 'partials/signUpAccount/signUpAccount.html',//暂无      					'zc': 'partials/mainManage/loyaltyManage.html',
      				'loyaltyManage': 'partials/mainManage/account/userEvent.html',//暂无
                    'userEvent': 'partials/mainManage/account/userEvent.html',
					'taskManage': 'partials/mainManage/taskManage/taskSystem.html',
        			'medalManage': 'partials/mainManage/medalManage/medalManage.html',//
    				'privilegeManage': 'partials/mainManage/privilege/privilegeManage.html',
        			'cardManage': 'partials/mainManage/cardManage/cardVoucherManage.html',
        			'gradeManage':	'partials/mainManage/gradeManage/gradeManage.html',//暂时用勋章代替
                    'activityOperate': 'partials/mainManage/activityOperate/activityOperate.html',
    				'operateStatics': 'partials/mainManage/operateStatics/operateStatics.html',
        			'warrantyManage': 'partials/mainManage/warrantyManage/warrantyManage.html',
        			'pushManage': 'partials/mainManage/pushManage/pushManage.html',
                    'kShopping': 'partials/mainManage/kShopping/kShopping.html'//暂无
    			};
                $rootScope.navTitle = {
                    'signUpAccount':'注册用户管理',
                    'loyaltyManage': '忠诚度管理',
                    'userEvent': '用户事件',
                    'taskManage': '任务管理',
                    'medalManage': '勋章管理',
                    'gradeManage': '等级管理',
                    'privilegeManage': '特权管理',
                    'cardManage': '卡券管理',
                    'activityOperate': '活动运营',
                    'operateStatics': '运营统计',
                    'kShopping': 'k点商城',
                    'warrantyManage': '报修管理',
                    'pushManage': '推送管理'
                };

                 $rootScope.navNextTitle = {
                    'signUpAccount':'注册用户管理',
                    'loyaltyManage': '忠诚度管理',
                    'userEvent': {
                        add: "新建事件",
                        edit: "编辑事件",
                        detail: "事件详情"
                    },
                    'taskManage': {
                        add: "新建任务",
                        edit: "编辑任务",
                        detail: "任务详情"
                    },
                    'medalManage': {
                        add: "新增勋章",
                        edit: "修改勋章",
                        detail: "勋章详情"
                    },
                    'gradeManage': {
                        add: "新增头衔",
                        edit: "修改头衔",
                        detail: "头衔详情"
                    },
                    'privilegeManage':{
                        add: "新增特权",
                        edit: "特权编辑",
                        detail: "特权详情"
                    },
                    'cardManage': {
                        add: "新增卡券",
                        edit: "修改卡券",
                        detail: "卡券详情"
                    },
                    'activityOperate':{
                        add: "新增活动",
                        edit: "活动编辑",
                        detail: "活动详情"
                    },
                    'operateStatics': {
                        add: "新增统计",
                        edit: "修改统计",
                        detail: "统计详情"
                    },
                    'kShopping': {
                        add: "新增商城",
                        edit: "修改商城",
                        detail: "商城详情"
                    },
                    'warrantyManage': {
                        add: "新增保修",
                        edit: "修改保修",
                        detail: "保修详情"
                    },
                    'pushManage': {
                        add: "新增推送",
                        edit: "修改推送",
                        detail: "推送详情"
                    }
                };
                $rootScope.jumpRoute = {
                    jumpToActive: function() {
                        $state.go('left.active', {
                            id: $state.params.id
                        })
                    },
                    jumpToEdit: function(nav) {
                        console.log(nav);
                        $state.go('left.active-edit',{
                            id: $state.params.id,
                            edit: nav
                        })
                    },
                    jumpToDetail: function(nav) {
                        $state.go('left.active-detail',{
                            id: $state.params.id,
                            type: nav
                        })
                    },
                    jumpToAdd: function(nav) {
                        $state.go('left.active-add',{
                            id: $state.params.id,
                            add: nav
                        })
                    }
                };


                $rootScope.leftNavChoose = 'loyaltyManage';
                $scope.secondLiChoice = 'userEvent';

        		$scope.chooseNav = function(e,nav){

        			$rootScope.leftNavChoose = nav.value;
    				$state.go('left.active',{
    					id: nav.value
    				})
        		};
                $scope.changeSecondLi = function(e, secondLi) {

                    e.stopPropagation();
                    $scope.secondLiChoice = secondLi;
                    // $rootScope.leftNavChoose = secondLi;
                    $state.go('left.active',{
                        id: secondLi
                    })
                };

            }]);
})();