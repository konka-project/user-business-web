/*
 * Directives
 */
'use strict';
(function() {
    var directiveModule = angular.module('myApp.rightEdit.directive', []);

    directiveModule.directive('rightEdit', function() {
        return {
            restrict: 'A',
            templateUrl:'partials/headNavManage/edit.html',
            controller: ['$scope',function($scope){

            }]
        };
    });
})();
