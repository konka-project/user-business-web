/*
 * Directives
 */
'use strict';
(function() {
    var directiveModule = angular.module('myApp.rightDetail.directive', []);

    directiveModule.directive('rightDetail', function() {
        return {
            restrict: 'A',
            templateUrl:'partials/headNavManage/detail.html',
            controller: ['$scope',function($scope){

            }]
        };
    });
})();
