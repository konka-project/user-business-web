'use strict';

(function() {
    var taskManage = function($scope,$state){
        var taskInitData = $scope.taskInitData = [];

        $scope.dataSize = {
            itemPerPage:15,
            bigTotalItems: 10,
            maxSize: parseInt(5),
            bigCurrentPage:1
        };

        $scope.changeLine = function(page) {
            $scope.dataSize.itemPerPage = page;
        };
        $scope.changePage = function(page) {
            console.log(page);
        };
        var data={
            taskLogo:"images/src/personal.jpg",
            taskId:"1237689437",
            taskName:"tasktask",
            taskCreateTime: new Date(),
        };
        $scope.loopData = function() {
            for(var i=1; i<100; i++) {
                taskInitData.push(data);
            }
        };
        $scope.loopData();

    };
    taskManage.$inject=['$scope', '$state'];
    angular.module('myApp.taskManage.controller',[])
        .controller('taskManage',taskManage);
})();