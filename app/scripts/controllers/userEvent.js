'use strict';

(function () {

    /**
     * @description 用户事件
     * @param $rootScope
     * @param $scope
     * @param $state
     * @param deleteSureService
     */
    var userEvent = function ($rootScope, $scope, $state, deleteSureService, successOperation) {
        $scope.data = [];
        $scope.dataOutside = [];
        $scope.dataSize = {
            itemPerPage:15,
            bigTotalItems: 10,
            maxSize: parseInt(5),
            bigCurrentPage:1
        };

        $scope.changeLine = function(page) {
            $scope.dataSize.itemPerPage = page;
        };
        $scope.changePage = function(page) {
            console.log(page);
        };
        for (var i = 1; i < 100; i++) {

            /**
             * @description 初始化数据
             * @type {{id: number, name: string, description: string, data: string, dateOrder: Date}}
             */
            $scope.dataInit = {
                id: i,
                name: "武汉时间" + i,
                description: "引起国民重大反应" + 9 * i,
                data: "12",
                dateOrder: new Date()
            };
            $scope.dataInit2 = {
                id: i,
                name: "北京时间" + i,
                description: "引起国民重大反应" + 9 * i,
                data: "12上大夫第三",
                dateOrder: new Date(),
            };
            $scope.data.push($scope.dataInit);
            $scope.dataOutside.push($scope.dataInit2);
        }
        $scope.eventChoose = [{
            name: "内部事件",
            value: "insideEvent"
        }, {
            name: "外部事件",
            value: "outsideEvent"
        }];

        $rootScope.chooseInOut = "insideEvent";
        $scope.chooseAction = function (e, nav) {

            e.stopPropagation();
            $rootScope.chooseInOut = nav.value;
        };

        $scope.createNewEvent = function () {
            /* 此处id为后台获取，新建事件 */
            $state.go('left.active-add', {
                id: $state.params.id,
                add: 'add'
            });
        };

        /* 删除 */
        $scope.deleteEventDetail = function (id) {
            deleteSureService.open("确定删除" + id + "事件吗？", "修改该事件后该事件就无法找回").then(function (res) {
                if (res) {
                    successOperation.open("删除成功");
                }
            }).then(function() {
                $state.go('left.active', {
                    id: $state.params.id
                })
            });
        };
    };

    userEvent.$inject = ['$rootScope', '$scope', '$state', 'deleteSureService','successOperation'];

    angular.module('myApp.userEvent.controller', [
            'myApp.delete.sure.modal'
        ])
        .controller('userEvent', userEvent);

})();