'use strict';
(function() {

	var privilegeManage = function($scope, $state) {
		$scope.privilegeInit = [];
		$scope.dataSize = {
			itemPerPage:15,
			bigTotalItems: 10,
			maxSize: parseInt(5),
			bigCurrentPage:1
		};

		$scope.changeLine = function(page) {
			$scope.dataSize.itemPerPage = page;
		};
		$scope.changePage = function(page) {
			console.log(page);
		};
		for(var i=300; i<550; i++) {
			var data = {
				picture:'images/src/personal.jpg',
				id: i,
				name: '生化'+i,
				create: new Date()
			}
			$scope.privilegeInit.push(data);
		}
	};
	privilegeManage.$inject = ['$scope', '$state'];
	angular.module('myApp.privilegeManage.controller',[])
		.controller('privilegeManage',privilegeManage);
})();

