'use strict';
(function() {
	var accountCheck = function ($rootScope, $scope, $state, deleteSureService, successOperation) {
		$rootScope.choiceRouter = $state.params.choice;


		$scope.remain = {
			eventNameSame:'与原有事件重名，请修改事件名称',
			eventNameNull:'请输入事件名称'
		};
		for(var i =1 ; i < 100; i++) {
			if($state.params.choice == i) {
				$scope.checkAfterDetail = {
					id: i,
					name: "北京时间" + i,
					descrition: "引起国民重大反应" + 9*i,
					data: "12上大夫第三",
					dateOrder: new Date(),
				};
			}
		}
		/*启动状态初始化*/
		var checkDataAdd = $scope.checkData = {};
		checkDataAdd.status = 'open';
		/*参数选择:*/
		$scope.optionInit = '';
		$scope.dataOption = [{
				name:'参数选择',
				value:''
			},{
				name:'文本',
				value: 1
			},{
				name:'日期',
				value: 2
			},{
				name:'字符串',
				value: 3
			},{
				name:'数值',
				value: 4
			},
		];
		/*添加事件*/
		$scope.eventItems = ['a'];
		$scope.addItem = function() {
			var x = $scope.eventItems.length+1;
			$scope.eventItems.push('a '+x);
		};
		$scope.reduceItem = function(item){

			var index = $scope.eventItems.indexOf(item);
			if (index > -1) {
				$scope.eventItems.splice(index, 1);
			}
		};
		/* 标签获取 */
		$scope.paramters = ["name", "sex", "change", "activity", "discount", "change"];
		// cacheService.setCacheData('paramters',["name", "sex", "change", "activity", "discount", "change"]);
		// cacheService.get('paramters');
        /* 删除 */
        $scope.deleteEventDetail = function (id) {
            deleteSureService.open("确定删除" + id + "事件吗？", "修改该事件后该事件就无法找回").then(function (res) {
                if (res) {
                    successOperation.open("删除成功");
                }
            }).then(function() {
                $state.go('left.active', {
                    id: $state.params.id
                })
            });
        };

		$scope.missionDetail = function(id) {
			$state.go('left.active-detail', {
				id: 'taskManage',
				type: id
			})
		};
		/* 操作后根据id 获取数据 */
		$scope.operateAfterData = {
			id: $state.params.type,
			name: '黑又壮',
			descrition: '事件描述：高大又威猛',
			createTime: new Date(),
			status: '禁用',
			eventData: {
				dataName: "黑又壮",
				dataType: "文本",
				dataDescription: "高大威猛"
			},
			connectWork: 'everyThing'
		};
	};
    accountCheck.$inject = ['$rootScope', '$scope', '$state', 'deleteSureService', 'successOperation'];

    angular.module("myApp.accountCheck.controller",[])
        .controller("accountCheck", accountCheck);
})();