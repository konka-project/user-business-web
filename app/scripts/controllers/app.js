'use strict';

(function () {
    angular.module('myApp.controllers', [
        'myApp.userEvent.controller',
        'myApp.accountCheck.controller',
        'myApp.medalManage.controller',
        'myApp.gradeManage.controller',
        'myApp.taskManage.controller',
        'myApp.privilegeManage.controller',
        'myApp.cardManage.controller',
        'myApp.activityOperate.controller',
        'myApp.rightMain.controller',
        'myApp.rightAdd.controller',
        'myApp.rightEdit.controller',
        'myApp.rightDetail.controller',
        'signUp.account.controller',
        'signUpAccountDetail.controller'
    ]);
})();