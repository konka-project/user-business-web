'use strict';
(function() {

	var rightMain = function($scope, $state) {
		$scope.activeId = {
			id: $state.params.id,
		}
	};
	rightMain.$inject = ['$scope', '$state'];
	angular.module('myApp.rightMain.controller', [])
		.controller('rightMain', rightMain);
})();

