'use strict';
(function() {

	var controllerModule = angular.module('myApp.rightAdd.controller', []);

	controllerModule.controller('rightAdd', ['$rootScope','$scope', '$state',function($rootScope, $scope,$state) {
	    	
	    $scope.activeId = {
			id: $state.params.id,
			jump: function() {
				$state.go('left.active', {
					id: $state.params.id,
				})
			}
		}

	
	}])
})();
