/**
 * Created by yangdeng on 16/9/3.
 */
'use strict';
(function () {
    
    var signUpAccount = function ($scope,$state) {

        $scope.list = [];

        for(var i = 0 ; i < 50 ;i ++){
            $scope.list.push(i);
        }

        $scope.showDetail = function (e) {
            e.stopPropagation();
            $state.go('left.signUnDetail');
        };

    };

    signUpAccount.$inject = ['$scope','$state'];

    angular.module('signUp.account.controller',[])
        .controller('signUpAccount',signUpAccount);
    
})();