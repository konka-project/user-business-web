'use strict';
(function() {
	var medalManage = function($scope, $state) {
		$scope.medalInitDate = [];
		$scope.medalDetailDate = {};
		$scope.dataSize = {
			itemPerPage:15,
			bigTotalItems: 10,
			maxSize: parseInt(5),
			bigCurrentPage:1
		};

		$scope.changeLine = function(page) {
			$scope.dataSize.itemPerPage = page;
		};
		$scope.changePage = function(page) {
			console.log(page);
		};
		var data = {
			id: '45',
			logo: 'images/src/personal.jpg',
			name: 'name'+$state.params.type,
			description: '及地方撒浪费大家撒浪费',
			status: '启用',
			create: new Date()
		};
		for(var i=500; i<900; i++) {
			$scope.medalInitDate.push(data);
		}
		$scope.medalDetailDate = data;
		console.log($scope.medalDetailDate);
	};
	medalManage.$inject = ['$scope', '$state'];
	angular.module('myApp.medalManage.controller', [])
		.controller('medalManage', medalManage);
})();
