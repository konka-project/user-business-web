/**
 * Created by yangdeng on 16/9/3.
 */
'use strict';
(function () {

    var signUpAccountDetail = function ($rootScope, $scope, deleteSureService, giveKPoint,editMedal) {

        $scope.chooseType = 'base';
        $scope.aignAccount = [];
        var data = {
            time: new Date(),
            type: '地方',
            content: 'dsadsafdsa',
            reason: 'sda'
        };
        for(var i=900; i<1000; i++) {
            $scope.aignAccount.push(data);
        }

        $scope.fn = {
            goBack: function (e) {
                e.stopPropagation();
            },
            changeTag: function (e, type) {
                e.stopPropagation();
                $scope.chooseType = type;
            },
            deleteKPoint: function (e) {
                e.stopPropagation();
                deleteSureService.open('确定清除该用户的所有K点吗?', '用户K点值将清零，无法购买相应商品');
            },
            giveKPiont: function (e) {
                e.stopPropagation();
                giveKPoint.open();
            },
            deleteMedal: function (e) {
                e.stopPropagation();
                deleteSureService.open('确定删除此勋章(富可敌国)吗?', '删除后，该用户无法享受该勋章相应的福利');
            },
            addMedal: function (e) {
                e.stopPropagation();
                editMedal.open();
            }

        };


    };

    signUpAccountDetail.$inject = ['$rootScope', '$scope', 'deleteSureService', 'giveKPoint','editMedal'];

    angular.module('signUpAccountDetail.controller', [])
        .controller('signUpAccountDetail', signUpAccountDetail);

})();