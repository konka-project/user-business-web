'use strict';

(function() {

    var cardManage = function($scope, $state) {
        var cardInintList = $scope.cardInintList = [];
        $scope.dataSize = {
            itemPerPage:15,
            bigTotalItems: 10,
            maxSize: parseInt(5),
            bigCurrentPage:1
        };

        $scope.changeLine = function(page) {
            $scope.dataSize.itemPerPage = page;
        };
        $scope.changePage = function(page) {
            console.log(page);
        };
        for(var i=100; i<300; i++) {
            var data = {
                id: i,
                name: '黑又壮'+i,
                type: '类型' + i,
                start: new Date(),
                end: new Date()
            };
            $scope.cardInintList.push(data);
        }
        $scope.cardDataDetail = {
            id: $state.params.type,
            name: '黑又壮'+$state.params.type,
            type: '类型' + $state.params.type,
            description:"一个小朋友",
            cardImg:'images/src/personal.jpg',
            multiple: 123,
            start: new Date(),
            effectLength: '1120分钟',
            create: new Date()
        }
    };

    cardManage.$inject = ['$scope', '$state'];
    angular.module('myApp.cardManage.controller',[])
        .controller('cardManage', cardManage);
})();