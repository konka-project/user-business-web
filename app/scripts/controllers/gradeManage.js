'use strict';
(function() {
	var gradeManage = function($scope, $state) {
		$scope.honorChoice = "honor";
		$scope.dataModal = [];
		$scope.logoDataModal =[];
		$scope.imgPathLogo = '';
		$scope.data = {
			imgLogoShows: ''
		};
		$scope.honorInit = [{
			name: "头衔管理",
			value: "honor"
		},{
			name: "图标管理",
			value: "logo"
		}];

		$scope.dataSize = {
			itemPerPage:15,
			bigTotalItems: 10,
			maxSize: parseInt(5),
			bigCurrentPage:1
		};

		$scope.changeLine = function(page) {
			$scope.dataSize.itemPerPage = page;
		};
		$scope.changePage = function(page) {
			console.log(page);
		};

		var data = $scope.data = {};
		$scope.honorLogoChoice = function(value) {

			$scope.honorChoice = value;
		};
		/* honor数据 */
		for( var i=3; i<100; i++) {
			$scope.honorInitData = {
				id: i,
				honor: 1+"y勋章",
				description: "描述信息",
				createTime: new Date()
			};
			$scope.dataModal.push($scope.honorInitData);
		}

		/* logo数据 */
		for( var i=300; i<400; i++) {
			$scope.honorInitData = {
				name: "陈剑"+i,
				picture: "images/logo/right-remain.png",
				description: i*1029 +478
			};
			$scope.logoDataModal.push($scope.honorInitData);
		}

		/**
		 * @description 监听上传图片机制,上传后,转换成浏览器的base64位展示出来
		 */
		$scope.checkEventDetail = function(e, dataDetail) {

			$scope.data.imgLogoShows = '';
			$("#myImgFileUploadModal").modal('show');
		};
		$scope.$watch('data.imgLogoShows',function() {

			$scope.imgPathLogo = $scope.getPath($("#fileUpload")[0]);
		});
		//返回选取的图片的路径
		$scope.getPath = function(file) {

			if(typeof($scope.data.imgLogoShows) == 'undefined' || $scope.data.imgLogoShows=='') {
				$scope.data.imgLogoShows = '';
				return $scope.imgPathLogo;
			}
			var ext=file.value.substring(file.value.lastIndexOf(".")+1).toLowerCase();
			if(ext != 'jpg'&& ext != 'jpeg'&&ext != 'png') {
				alert("您上传的图片格式不正确！");
				$scope.data.imgLogoShows = '';
				return $scope.imgPathLogo;
			}
			var isIE = navigator.userAgent.match(/MSIE/)!= null,
				isIE6 = navigator.userAgent.match(/MSIE 6.0/)!= null;
			if(isIE) {
				file.select();
				var reallocalpath = document.selection.createRange().text;

				// IE6浏览器设置img的src为本地路径可以直接显示图片
				if (isIE6) {
					pic.src = reallocalpath;
				}else {
					// 非IE6版本的IE由于安全问题直接设置img的src无法显示本地图片，但是可以通过滤镜来实现
					pic.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod='image',src=\"" + reallocalpath + "\")";
					// 设置img的src为base64编码的透明图片 取消显示浏览器默认图片
					pic.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
				}
			}else {

				var fileNext = file.files[0];
				var reader = new FileReader();
				reader.readAsDataURL(fileNext);
				reader.onload = function(e){
					$scope.imgPathLogo  = this.result;
					$scope.$apply(function($scope){
						return $scope.imgPathLogo;
					});

				}
			}
		};
		/* 后台获取编辑添加等数据 */
		$scope.dataIntroduction = {

			edit: function(nav) {
				var data = {
					id: nav,
					name:"岑寂",
					description:'dsaf'
				}
				return data;
			},
			detail: function(nav) {
				var data = {
					id: nav,
					name:"岑寂detail",
					description: "dsfdsafd",
					createTime: new Date()
				}
				return data;
			}
		};
		$scope.dataIntroductionEdit = $scope.dataIntroduction.edit($state.params.edit);
		$scope.dataIntroductionDetail = $scope.dataIntroduction.detail($state.params.type);
	};
	gradeManage.$inject = ['$scope', '$state'];
	angular.module('myApp.gradeManage.controller',[])
		.controller('gradeManage', gradeManage);
})();